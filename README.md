LZFX compression library 0.1
============================

Downloads, bug tracker, documentation are at Google Code:
http://lzfx.googlecode.com

Contact email: _andrew **dot** collette **at** gmail **dot** com_

What is LZFX?
=============

LZFX is a tiny, extremely fast compression library compatible with liblzf.
It was originally developed in support of the HDF5 for Python project
([h5py.googlecode.com](http://h5py.googlecode.com)).

The goal of LZFX is to provide high-speed compression of redundant data.
Compression ratios are therefore not as good as other compression utilities
like gzip or bzip.  Until fast RLE encoders, LZFX uses dictionary-style
compression allowing it to deal with arbitrary repeated substrings, not just
runs of identical characters.

How do I use it?
================

The library consists of two files, *lzfx.c* and *lzfx.h*.  Please read *lzfx.h* for
API information.  Like LZF, you simply supply an input and output buffer.
There are no compression settings to adjust.  You can link against *liblzfx.a*
or simply copy the two source files into your project.  Since LZFX is BSD
licensed, these approaches are legal for both open-source and proprietary
applications.

Acknowledgements
================

LZFX is based on the LZF code base by Marc Lehmann.  Changes include a new
API to make it easier to use with HDF5/h5py, code refactoring, and changes
to the decompressor to make it easier to determine the required size of a 
decompression buffer.  The LZFX compressor and decompressor are 100% compatible
with existing LZF streams, although the file format used by the "lzfx" demo
utility is different than that used by the "lzf" utility.

License
=======

LZFX is copyright (c) 2009 Andrew Collette and subject to the BSD license
(below).  Original LZF copyright statement follows.

Copyright (c) 2000-2007 Marc Alexander Lehmann <schmorp@schmorp.de>

Redistribution and use in source and binary forms, with or without modifica-
tion, are permitted provided that the following conditions are met:

  1.  Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.

  2.  Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MER-
CHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPE-
CIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTH-
ERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE.
